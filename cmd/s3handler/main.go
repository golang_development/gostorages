package main

import (
	"context"
	"log"
	"os"
	"strings"

	"gitlab.com/golang_development/gostorages/fs"
)

func main() {
	var ctx context.Context

	dir := os.TempDir()
	log.Println(dir)
	storage := fs.NewStorage(fs.Config{Root: dir})

	// Saving a file named test
	storage.Save(ctx, strings.NewReader("Hello word"), "test")

	// Deleting the new file on the storage
	// storage.Delete(ctx, "test")

}
