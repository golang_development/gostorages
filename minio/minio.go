package minio

import (
	"context"
	"fmt"
	"io"
	"log"
	"os"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"github.com/pkg/errors"
	"gitlab.com/golang_development/gostorages"
)

type Config struct {
	Endpoint        string
	Basket string
	AccessKeyID     string
	SecretAccessKey string
	UseSSL          bool
}
type Storage struct {
	bucket string
	client *minio.Client
}

// NewStorage returns a new Storage.
func NewStorage(conf Config) (*Storage, error) {

	// Initialize minio client object.
	minioClient, err := minio.New(conf.Endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(conf.AccessKeyID, conf.SecretAccessKey, ""),
		Secure: conf.UseSSL,
	})
	if err != nil {
		log.Fatalln(err)
	}

	return &Storage{client: minioClient, bucket:conf.Basket}, nil
}

func (g *Storage) Save(ctx context.Context, content io.Reader, path string) error {

	err := g.client.MakeBucket(ctx, g.bucket, minio.MakeBucketOptions{})
	if err != nil {
		// Check to see if we already own this bucket (which happens if you run this twice)
		exists, errBucketExists := g.client.BucketExists(ctx, g.bucket)
		if errBucketExists == nil && exists {
			log.Printf("We already own %s\n", g.bucket)
		} else {
			log.Fatalln(err)
		}
	} else {
		log.Printf("Successfully created %s\n", g.bucket)
	}

	contentType := "application/octet-stream"

	// Upload the test file with FPutObject
	_, err = g.client.PutObject(ctx, g.bucket, path, content, -1, minio.PutObjectOptions{ContentType: contentType})
	if err != nil {
		log.Fatalln(err)
	}

	return nil
}

// Stat returns path metadata.
func (g *Storage) Stat(ctx context.Context, path string) (*gostorages.Stat, error) {

	r, err := g.client.StatObject(ctx, g.bucket, path, minio.GetObjectOptions{})
	if err != nil {
		return nil, err
	}

	return &gostorages.Stat{
		ModifiedTime: r.LastModified,
		Size:         r.Size,
	}, nil
}

// // Open opens path for reading.
func (g *Storage) Open(ctx context.Context, path string) (io.ReadCloser, error) {


	r, err := g.client.GetObject(ctx, g.bucket, path, minio.GetObjectOptions{})
	if err != nil {
		return nil, err
	}
	return r, errors.WithStack(err)
}

// Delete deletes path.
func (g *Storage) Delete(ctx context.Context, path string) error {
	// return errors.WithStack(g.bucket.Object(path).Delete(ctx))
	err := g.client.RemoveObject(ctx, g.bucket, path, minio.RemoveObjectOptions{})
	if err != nil {
		return  err
	}
	return errors.WithStack(err)
}

// OpenWithStat opens path for reading with file stats.
func (g *Storage) OpenWithStat(ctx context.Context, path string) (io.ReadCloser, *gostorages.Stat, error) {

	log.Println("OpenWithStat BASKET!!!",g.bucket,"PATH!!",path)


	err := g.client.MakeBucket(ctx, g.bucket, minio.MakeBucketOptions{})
	if err != nil {
		// Check to see if we already own this bucket (which happens if you run this twice)
		exists, errBucketExists := g.client.BucketExists(ctx, g.bucket)
		if errBucketExists == nil && exists {
			log.Printf("We already own %s\n", g.bucket)
		} else {
			log.Fatalln(err)
		}
	} else {
		log.Printf("Successfully created %s\n", g.bucket)
	}

	
	r, err := g.client.GetObject(ctx, g.bucket, path, minio.GetObjectOptions{})
	if err != nil {
		return nil,nil, err
	}


	defer r.Close()

	localFile, err := os.Create("/tmp/local-file.jpg")
	if err != nil {
		fmt.Println(err)
		return nil,nil, err
	}
	// defer localFile.Close()

	if _, err = io.Copy(localFile, r); err != nil {
		fmt.Println(err)
		return nil,nil, err
	}

	f, err := os.Open("/tmp/local-file.jpg")
	if os.IsNotExist(err) {
		return nil, nil, gostorages.ErrNotExist
	}

	stat, err := r.Stat()
	if err != nil {
		return nil,nil, err
	}
	return f, &gostorages.Stat{
		ModifiedTime: stat.LastModified,
		Size:         stat.Size,
	}, errors.WithStack(err)
}
